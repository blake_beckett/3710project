# README #


### What is this repository for? ###

* This repository is for Project SPOON, a game project by students at the University of Utah. This game was developed over 3 months as part of a Traditional Game Development course.


### The Team ###
- Blake Beckett - Programming, Gameplay Design, Music
- Jayce Keller - 3D modelling, Concept Art, Particles
- Jack Corrigan - Level Design, Sound Design
- Grant Bernstein - 3D Modelling, Textures, Music

### What Exactly is Project SPOON? ###
Project SPOON is a top-down, competitive,  2.5D space dogfighting game, but with a unique movement mechanic. Ships have no thrust capabilities in Project SPOON, so grappling points throughout space must be used to swing around the map and get the drop on the enemy ship. Confused? Check out some footage of the game [here](https://drive.google.com/file/d/0BwQ1ApUk5zoRdzFfb29KNVMxcGc/view?usp=sharing).

### Where is Everything Located? ###
All Assets used in the game can be found in the Assets Folder. All Code is located in /Assets/Scripts.

- NOTE: This project uses XInputDotNet for Xbox 360 gamepad support, check it out on [github](https://github.com/speps/XInputDotNet). Some XInputDotNet scripts can be found in the scripts folder.

### Features ###
* Engaging, one on one combat
* An intuitive dual joystick shooter control scheme (Xinput game pads required)
* 3 ships and 3 weapons to choose from , each with unique strengths and weaknesses
* 3 maps with carefully selected grapple point layouts