﻿using UnityEngine;
using System.Collections;

public class ScoreKeeper : MonoBehaviour 
{	
	public int player1Score;
	public int player2Score;
	public int winScore;
	public static string winner;
	public AudioClip song1;
	public AudioClip song2;
	public AudioClip song3;
	public Font font;

	private GUIText scoreBoard;
	private static bool created = false;
	private AudioSource src;

	void Start () 
	{
		if (!created) 
		{
			// this is the first instance - make it persist
			DontDestroyOnLoad(this.gameObject);
			created = true;
		} 
		else 
		{
			// this must be a duplicate from a scene reload - DESTROY!
			Destroy(this.gameObject);
		} 
		
		src = this.gameObject.AddComponent<AudioSource>();
		
		if(MenuController.selectedLevel == "Basic")
		{
			src.clip = song2;
			src.volume = 1f;
			src.loop = true;
			src.Play();
		}
		else if (MenuController.selectedLevel == "Chaos")
		{
			src.clip = song3;
			src.volume = 0.9f;
			src.loop = true;
			src.Play();
		}
		else
		{
			src.clip = song1;
			src.volume = 0.7f;
			src.loop = true;
			src.Play();
		}
			
		
		scoreBoard = GetComponent<GUIText> ();
		scoreBoard.font = font;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (player1Score >= winScore) 
		{
			winner = "Player 1";
			StartCoroutine(GameOver(winner));
		} 
		else if (player2Score >= winScore) 
		{
			winner = "Player 2";
			StartCoroutine(GameOver(winner));
		}

		scoreBoard.text = "P1: " + player1Score + "   |   P2: " + player2Score;
	}

	IEnumerator GameOver(string winner)
	{
		player1Score = 0;
		player2Score = 0;
		
		yield return new WaitForSeconds(2f);
		
		//src.Stop();
		Object.Destroy(gameObject);
		created = false;
		
		Application.LoadLevel ("WinScreen");

	}
}
