﻿using UnityEngine;
using System.Collections;

public class WinScreenScript : MonoBehaviour 
{
	private GUIText text;

	void Start () 
	{
		text = GetComponent<GUIText> ();
		text.text = ScoreKeeper.winner + " Wins!!!\n";
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown (KeyCode.R)) 
		{
			Application.LoadLevel (MenuController.selectedLevel);
		}
		
		if (Input.GetKeyDown (KeyCode.M)) 
		{
			Application.LoadLevel ("Menu1");
		}
	}
}
