﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MenuController : MonoBehaviour 
{
	public static string Player1Weapon;
	public static string Player2Weapon;
	public static string Player1Chassis;
	public static string Player2Chassis;
	public static int roundsToWin;
	public static string selectedLevel;
	public Text p1ChassisText;
	public Text p1WeaponText;
	public Text p2ChassisText;
	public Text p2WeaponText;
	public Text mapText;
	public Text roundText;
	

	// Use this for initialization
	void Start () 
	{
		//Set the defaults for each option
		Player1Weapon = "MachineGun";
		Player2Weapon = "MachineGun";
		Player1Chassis = "Medium";
		Player2Chassis = "Medium";
		selectedLevel = "Basic";
		roundsToWin = 3;
	}
	
	public void SetP1Ship(string shipChassis)
	{
		Player1Chassis = shipChassis;
		p1ChassisText.text = shipChassis;
	}
	
	public void SetP2Ship(string shipChassis)
	{
		Player2Chassis = shipChassis;
		p2ChassisText.text = shipChassis;
	}
	
	public void SetP1Weapon(string weapon)
	{
		Player1Weapon = weapon;
		p1WeaponText.text = weapon;
	}
	
	public void SetP2Weapon(string weapon)
	{
		Player2Weapon = weapon;
		p2WeaponText.text = weapon;
	}
	
	public void SetRoundNum(int rounds)
	{
		roundsToWin = rounds;
		roundText.text = rounds.ToString();
	}
	public void QuitGame()
	{
		Application.Quit();
	}
	
	public void SetLevel(string levelName)
	{
		selectedLevel = levelName;
		mapText.text = levelName;
	}
	
	public void LoadLevel()
	{
		Application.LoadLevel(selectedLevel);
	}
	
	public void LoadMenu()
	{
		Application.LoadLevel("Menu1");
	}
	
	public void LoadTutorial()
	{
		Application.LoadLevel("Tutorial1");
	}
	
	public void LoadTutorial2()
	{
		Application.LoadLevel("Tutorial2");
	}
	
	public void LoadTutorial3()
	{
		Application.LoadLevel("Tutorial3");
	}
}
