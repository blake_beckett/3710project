﻿using UnityEngine;
using XInputDotNetPure;
using System.Collections;

public class IntroScript : MonoBehaviour 
{

	private GamePadState p1CurrentState;
	private GamePadState p2CurrentState;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		p1CurrentState = GamePad.GetState(PlayerIndex.One, GamePadDeadZone.None);
		p2CurrentState = GamePad.GetState(PlayerIndex.Two, GamePadDeadZone.None);
		
		if(p1CurrentState.Buttons.A == ButtonState.Pressed || p1CurrentState.Buttons.Start == ButtonState.Pressed || 
			p2CurrentState.Buttons.A == ButtonState.Pressed || p2CurrentState.Buttons.Start == ButtonState.Pressed)
		{
			Application.LoadLevel("Menu1");
		}
		
	}
}
