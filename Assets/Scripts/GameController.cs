﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour 
{
	public GameObject Player1;
	public GameObject Player2;
	public GUIText countdown;
	public Slider player1HealthBar;
	public Slider player1ShieldBar;
	public Slider player2HealthBar;
	public Slider player2ShieldBar;
	public float barDepletionRate;
	public AudioClip explosionSound;

	private PlayerController player1Script;
	private PlayerController player2Script;
	private ScoreKeeper scoreKeeperScript;
	private float countdownTime;
	private bool countingDown;
	private bool roundOver = false;
	private float startTime;
	private string selectedMap;


	// Use this for initialization
	void Start () 
	{
		roundOver = false;
		player1Script = Player1.GetComponent<PlayerController> ();
		player2Script = Player2.GetComponent<PlayerController> ();
		scoreKeeperScript = GameObject.Find("Score Keeper").GetComponent<ScoreKeeper>();		
		selectedMap = MenuController.selectedLevel;

		Player1.SetActive(false);
		Player2.SetActive(false);

		StartCoroutine (CountdownToStart());
	}

	IEnumerator CountdownToStart()
	{
		var displayTime = 1f;

		countdown.text = "3";
		yield return new WaitForSeconds (displayTime);  

		countdown.text = "2";
		yield return new WaitForSeconds (displayTime); 
		
		countdown.text = "1";
		yield return new WaitForSeconds (displayTime); 
		
		countdown.text = "Start";
		yield return new WaitForSeconds (displayTime); 

		countdown.enabled = false; 

		Player1.SetActive(true);
		Player2.SetActive(true);
	}
	
	private float UpdateBar(float prevVal, float currVal)
	{
		if(prevVal > currVal)
		{
			return Mathf.Clamp((prevVal - barDepletionRate), currVal, float.PositiveInfinity);
		}
		else
			return currVal;
	}
	

	// Update is called once per frame
	void Update () 
	{
		player1HealthBar.maxValue = player1Script.maxHealth;
		player2HealthBar.maxValue = player2Script.maxHealth;
		
		player1HealthBar.value = UpdateBar(player1HealthBar.value, player1Script.Health);
		player1ShieldBar.value = UpdateBar(player1ShieldBar.value, player1Script.Shields);
		player2HealthBar.value = UpdateBar(player2HealthBar.value, player2Script.Health);
		player2ShieldBar.value = UpdateBar(player2ShieldBar.value, player2Script.Shields);
			
		if (!roundOver && player1Script.Destroyed) 
		{
			scoreKeeperScript.player2Score++;
			roundOver = true;
			StartCoroutine(EndRound("Player 2"));
			
			AudioSource src = this.gameObject.AddComponent<AudioSource>();
			src.clip = explosionSound;
			src.Play();
			
			//EndRound("Player 2");

		} 
		else if (!roundOver && player2Script.Destroyed ) 
		{
			scoreKeeperScript.player1Score++;
			roundOver = true;
			StartCoroutine(EndRound("Player 1"));
			
			AudioSource src = this.gameObject.AddComponent<AudioSource>();
			src.clip = explosionSound;
			src.Play();
			
			//EndRound("Player 1");
		}
			
	}
	
	IEnumerator EndRound(string winner)
	{
		countdown.enabled = true;
		countdown.text = winner + " Wins the Round!";
		yield return new WaitForSeconds(2.5f);

		countdown.enabled = false;
		Application.LoadLevel (selectedMap);
	}
}