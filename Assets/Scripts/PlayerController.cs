using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using XInputDotNetPure;

public class PlayerController : MonoBehaviour 
{
	//Public members that we can set for balance, playability, fun-ness, etc.
	public string playerName;
	public PlayerIndex playerIndex;
	public Vector3 startPosition;
	public Vector3 startDirection;
	public GameObject turret;
	public GameObject grappleIndic;
	public GameObject aimRing;
	public GameObject projectile;
	public GameObject placeHolderGrapple;
	public GameObject shipExplosionEffect;
	public GameObject wallCollisionEffect;
	public GameObject shieldRechargeEffect;
	public GameObject shipHitEffect;
	public Mesh lightChassis;
	public Mesh heavyChassis;
	public Mesh shotgunModel;
	public Mesh railgunModel;
	public Material[] shipTextures;
	public Material[] turretTextures;
	public LineRenderer grappleRender;
	public LineRenderer railgunAim;
	public Transform projectileSpawn;
	public float speed;
	public float angleScale;
	public float grappleDistRange;
	public float grappleAngleRange;
	public float maxHealth;
	public float maxShields;
	public float shieldRechargeDelay;
	public float shieldRechargeRate;
	public float gunDisableTime;
	public float shotgunSpread;
	public float shotgunDamage;
	public float machineGunDamage;
	public float minRailgunDamage;
	public float maxRailgunDamage;
	public float railgunChargeRate;
	public float joystickAngularVelocity;
	public AudioClip[] AudioClips;

	//Private members that are needed throughout the class, but that don't affect gameplay/balance
	private GameObject currentGrapple;
	private GameObject projectileInstance;
	private GameObject shieldInstance;
	private Rigidbody rigBody;
	private Vector3 direction;
	private bool lTriggerHeld;
	private bool destroyed = false;
	private float currentHealth;
	private float currentShields;
	private float prevShields;
	private float distToShip;
	private float startTime;
	private float shieldHitTime;
	private float fireRate;
	private float nextFire = 0.0f;
	private int orbitDirection;
	private float countdownTime;
	private string weaponType;
	private string chassis;
	private float railgunDamage;
	private float railgunCharge;
	private GameObject shipModel;
	private GameObject turretModel;
	private ShipState shipState;
	private RailgunState rState;
	private AudioSource gunSource;
	private AudioSource shieldSource;
	private AudioSource otherSource;
	
	enum ShipState
	{
		Flying, 
		Rotating, 
		GrappleFired, 
		GrappleReleased
	}
	
	enum RailgunState
	{
		Starting, 
		Charging, 
		Firing, 
		Waiting
	}
	
	GamePadState previousState;
	GamePadState currentState;
	
	Vector3 rightStick;
	Vector3 leftStick;
	
	/****************************KNOWN BUGS*************************
	* Add bugs to the list when you encounter them, remove them once you 
	* code the fix and you are confident that the bug is eliminated.
	*
	* Sometimes the ship stays grappled upon release of left trigger - Maybe gone, not sure.
	* 
	* Laser aim stays when you gtrapple whie chargig the railgun
	* 
	* Ship classes should have different health instead of shields.
	* 
	* Occassionally, the grapple aim pointer will change sides when is it isn't supposed to - minor
	*/

	
	void Start () 
	{
		countdownTime = 3;
		startTime = Time.time;

		shipModel = this.transform.Find("ShipModel").gameObject;
		turretModel = this.transform.Find("Turret").transform.Find("TurretModel").gameObject;

		transform.position = startPosition;
		currentGrapple = placeHolderGrapple;
		shipState = ShipState.GrappleReleased;	
		rState = RailgunState.Waiting;
		
		gunSource = this.gameObject.AddComponent<AudioSource>();
		shieldSource = this.gameObject.AddComponent<AudioSource>();
		otherSource = this.gameObject.AddComponent<AudioSource>();	
		
		if(playerName == "Player1")
		{
			weaponType = MenuController.Player1Weapon;
			chassis = MenuController.Player1Chassis;
		}
		else
		{
			weaponType = MenuController.Player2Weapon;
			chassis = MenuController.Player2Chassis;
		}
		
		Material shipTexture;
		
		if(chassis == "Light")
		{
			shipModel.GetComponent<MeshFilter>().mesh = lightChassis;
			shipModel.GetComponent<SphereCollider>().radius = 1f;
			
			if(playerIndex == PlayerIndex.One)
				shipTexture = shipTextures[0];
			else
				shipTexture = shipTextures[3];
			
			shipModel.GetComponent<MeshRenderer>().material = shipTexture;
			
			maxHealth = 80;
			speed = speed * 1.2f;
		}
		else if(chassis == "Heavy")
		{
			shipModel.GetComponent<MeshFilter>().mesh = heavyChassis;
			shipModel.GetComponent<SphereCollider>().radius = 1.8f;
			shipModel.GetComponent<SphereCollider>().center = new Vector3(0f, -0.3f, 0f);
			
			if(playerIndex == PlayerIndex.One)
				shipTexture = shipTextures[2];
			else
				shipTexture = shipTextures[5];
			
			shipModel.GetComponent<MeshRenderer>().material = shipTexture;
			
			maxHealth = 120;
			speed = speed * 0.8f;
		}
		else
		{
			if(playerIndex == PlayerIndex.One)
				shipTexture = shipTextures[1];
			else
				shipTexture = shipTextures[4];	
			shipModel.GetComponent<MeshRenderer>().material = shipTexture;
		}
		
		Material gunTexture;
		
		switch(weaponType)
		{			
			case "Shotgun":
				turretModel.GetComponent<MeshFilter>().mesh = shotgunModel;
				if(playerIndex == PlayerIndex.One)
					gunTexture = turretTextures[1];
				else
					gunTexture = turretTextures[4];	
				
				turretModel.GetComponent<MeshRenderer>().material = gunTexture;
				fireRate = 0.8f;
				break;
				
			case "MachineGun":
				if(playerIndex == PlayerIndex.One)
					gunTexture = turretTextures[0];
				else
					gunTexture = turretTextures[3];	
				
				turretModel.GetComponent<MeshRenderer>().material = gunTexture;
				fireRate = 0.25f;
				break;
				
			case "Railgun":
				turretModel.GetComponent<MeshFilter>().mesh = railgunModel;
				if(playerIndex == PlayerIndex.One)
					gunTexture = turretTextures[2];
				else
					gunTexture = turretTextures[5];	
				
				turretModel.GetComponent<MeshRenderer>().material = gunTexture;
				fireRate = 1.5f;
				break;
			
			default:
				fireRate = 0.25f;
				break;
		}
		
		currentHealth = maxHealth;
		currentShields = maxShields;
		
		railgunAim.enabled = false;

		//Store the ridigbody in a  variable so we can access it easily later.
		//Then, apply and instant velocity to the ship (toward the right of the screen)
		rigBody = GetComponent<Rigidbody>();
		rigBody.freezeRotation = true;
		
		//grappleIndic.transform.LookAt(Vector3.right, Vector3.forward);
		
		direction = startDirection * speed;
		rigBody.AddForce (direction, ForceMode.VelocityChange);
	}

	public float Shields
	{
		get {return currentShields;}
		set {currentHealth = value;}
	}

	public float Health
	{
		get {return currentHealth;}
		set {currentHealth = value;}
	}

	public bool Destroyed
	{
		get{return destroyed;}
	}

	public float CountdownTime
	{
		get{return countdownTime;}
	}

	void FixedUpdate () 
	{
		grappleRender.enabled = false;
		
		if(shipState == ShipState.Flying)
		{
			grappleIndic.GetComponentInChildren<Renderer>().enabled = true;
			aimRing.GetComponent<Renderer>().enabled = true;		
		}

			

		if(rightStick.magnitude > 0.25)
		{
			var currentRotation = Quaternion.LookRotation(rightStick, Vector3.up);
			turret.transform.rotation = Quaternion.Lerp(turret.transform.rotation, currentRotation, Time.deltaTime * joystickAngularVelocity);
		}
		
		if(leftStick.magnitude > 0.25)
		{
			grappleIndic.transform.LookAt(grappleIndic.transform.position + leftStick, Vector3.forward);
		}

		//Find the vectors that point directly to the right and left of the ship
		//We use these to restrict the angle that we can fire the grapple
		var right = transform.right.normalized;
		var left = right * -1;

		var rightAngleDiff = Vector3.Angle (grappleIndic.transform.forward.normalized, right);
		var leftAngleDiff = Vector3.Angle (grappleIndic.transform.forward.normalized, left);
		
		if(rightAngleDiff <= leftAngleDiff)
		{
			grappleIndic.transform.LookAt(grappleIndic.transform.position + right, Vector3.forward);
		}
		else
		{
			grappleIndic.transform.LookAt(grappleIndic.transform.position + left, Vector3.forward);
		}

		if (shipState == ShipState.GrappleReleased) 
		{
			if(currentGrapple == placeHolderGrapple)
			{
				shipState = ShipState.Flying;
			}
			else
			{
				//Calculate thee vector that is perpendicular to the vector that points from the grapple point to the ship
				//then limit its magnitude to 1 so that the speed is uniform. 
				var perpVector = Vector3.Cross ((transform.position - currentGrapple.transform.position), transform.up);
				direction = perpVector.normalized * -orbitDirection;
				transform.forward = direction;
				
				//Use the vector calculated above to add instant velocity to the ship in the desired direction
				rigBody.velocity = Vector3.zero; //remove any forces from the ship
				rigBody.AddForce(direction * speed, ForceMode.VelocityChange);
				
				grappleIndic.GetComponentInChildren<Renderer>().enabled = true;
				aimRing.GetComponent<Renderer>().enabled = true;
				
				shipState = ShipState.Flying;
			}
		}

		if (shipState == ShipState.GrappleFired) 
		{
			//This grapple point is used to avoid null pointer exceptions
			currentGrapple = placeHolderGrapple;
			RaycastHit grappleHit;
			
				if (Physics.Raycast (transform.position, grappleIndic.transform.forward, out grappleHit, grappleDistRange)) {
					if (grappleHit.transform.gameObject.tag == "GrapplePoint" && lTriggerHeld) {
						currentGrapple = grappleHit.transform.gameObject;
					}
				}

			//If currentGrapple was set to something new, then a valid grapple was found, and we can rotate around it
			if(currentGrapple != placeHolderGrapple)
			{
				shipState = ShipState.Rotating;
				
				PlaySound(gunSource, 5);
				var pointToGrapple = (currentGrapple.transform.position - transform.position);
				rightAngleDiff = Vector3.Angle (pointToGrapple.normalized, right);
				leftAngleDiff = Vector3.Angle (pointToGrapple.normalized, left);
				distToShip = pointToGrapple.magnitude;

				//Determine if the grapple point is one the right or left, and set the direction of orbit accordingly.
				if(rightAngleDiff < leftAngleDiff)
				{
					orbitDirection = 1;
				}
				else
				{
					orbitDirection = -1;
				}

			}
		}

		if (shipState == ShipState.Rotating) 
		{		
			grappleIndic.GetComponentInChildren<Renderer>().enabled = false;
			aimRing.GetComponent<Renderer>().enabled = false;
			
			//Use the distance from the ship to calculate the proper delta angle so that the speed of 
			//rotation around the grapple point is consistent with the speed of the ship's movement.
			var angle = (angleScale / distToShip) * speed * orbitDirection;
			rigBody.velocity = Vector3.zero; //remove any forces from the ship
			transform.RotateAround (currentGrapple.transform.position, currentGrapple.transform.up, angle); //Rotate around whichever grapple point is selected

			//Re-orient the ship so that its forward vector is exactly the same as the vector perpendicular to the circle about which it is rotating
			var perpVector = Vector3.Cross ((transform.position - currentGrapple.transform.position), transform.up);
			direction = perpVector.normalized * -orbitDirection;
			//transform.forward = direction;
			transform.forward = Vector3.Lerp(transform.forward, direction, 0.5f);

			//Draw the grapple line to the screen using a line renderer.
			grappleRender.enabled = true;
			grappleRender.SetPosition(0, transform.position);
			grappleRender.SetPosition(1, currentGrapple.transform.position);
		}
		
		//Adjust the ships transfrom so that is alwasy has y position 0
		Vector3 adjustedPos = new Vector3(transform.position.x, 0, transform.position.z);
		transform.position = adjustedPos;
		
		foreach(GameObject shot in GameObject.FindGameObjectsWithTag("Projectile"))
		{
			var fixedPos = new Vector3(shot.transform.position.x, 0, shot.transform.position.z);
			shot.transform.position = fixedPos;
		}
		
		grappleIndic.transform.rotation = Quaternion.Euler(grappleIndic.transform.rotation.eulerAngles.x, grappleIndic.transform.rotation.eulerAngles.y, 270);
	}

	void Update()
	{
		GetInput();
		
		if((currentShields < maxShields) && (Time.time - shieldHitTime >= shieldRechargeDelay))
		{
			currentShields = currentShields + shieldRechargeRate;
		}
		
		if(prevShields <= 0 && currentShields > 0)
		{
			shieldInstance = Instantiate (shieldRechargeEffect, this.transform.position, this.transform.rotation) as GameObject;
			shieldSource.volume = 0.7f;
			shieldSource.pitch = 1.5f;
			PlaySound(shieldSource, 7);
		}
		
		if (prevShields > 0 && currentShields <= 0)
		{
			shieldSource.volume = 1f;
			shieldSource.pitch = 1f;
			PlaySound(shieldSource, 9);
			
			if(shieldInstance.activeSelf)
			{
				Object.Destroy(shieldInstance.gameObject);
			}
		}
		
		if((shieldInstance != null) && shieldInstance.activeSelf)
		{
			shieldInstance.transform.position = this.transform.position;
		}
		
		if (currentHealth <= 0) 
		{
			this.destroyed = true;
			Instantiate(shipExplosionEffect, this.transform.position, this.transform.rotation);
			Object.Destroy(this.gameObject);
		}

		currentHealth = Mathf.Clamp (currentHealth, 0, maxHealth);
		currentShields = Mathf.Clamp (currentShields, 0, maxShields);
		
		prevShields = currentShields;
	}
	
	void GetInput()
	{
		currentState = GamePad.GetState(playerIndex, GamePadDeadZone.None);
		
		railgunAim.enabled = false;	
			
		if(!currentState.IsConnected)
			return;

		rightStick.x = currentState.ThumbSticks.Right.X;
		rightStick.y = turret.transform.position.y;
		rightStick.z = currentState.ThumbSticks.Right.Y;
		
		leftStick.x = currentState.ThumbSticks.Left.X;
		leftStick.y = 0.0f;
		leftStick.z = currentState.ThumbSticks.Left.Y;
		
		if((previousState.Triggers.Left <= 0.25) && (currentState.Triggers.Left > 0.25))
		{
			lTriggerHeld = true;
			shipState = ShipState.GrappleFired;
		}
		else if((previousState.Triggers.Left > 0.25) && (currentState.Triggers.Left <= 0.25))
		{
			shipState = ShipState.GrappleReleased;	
			lTriggerHeld = false;
		}
		
		if(weaponType == "Railgun" && previousState.Triggers.Right > 0 && currentState.Triggers.Right <= 0 && rState == RailgunState.Charging)
		{
			railgunDamage = Mathf.Clamp(railgunCharge, minRailgunDamage, maxRailgunDamage);
			nextFire = Time.time + fireRate;
			//Debug.Log("Non-Fully Charged Shot Fired: " + railgunDamage);
			FireWeapon(railgunDamage);
			railgunAim.enabled = false;
			
			rState = RailgunState.Waiting;

		}

		if((shipState != ShipState.Rotating)  && ((Time.time - startTime) > gunDisableTime) && (Time.time > nextFire))
		{
			if(weaponType == "Railgun" && (currentState.Triggers.Right > 0))
			{
				HandleRailgun();
			}		
			else if ((currentState.Triggers.Right > 0))
			{
				nextFire = Time.time + fireRate;
				float shotDamage = 0;
				switch(weaponType)
				{
					case "MachineGun":
						shotDamage = machineGunDamage;
						break;
						
					case "Shotgun":
						shotDamage = shotgunDamage;
						break;
				}
				
				FireWeapon(shotDamage);
			}
		}
		previousState = currentState;
	}
	
	void HandleRailgun()
	{
		if(rState == RailgunState.Waiting)
		{
			railgunCharge = minRailgunDamage;
			PlaySound(gunSource, 6);
			rState = RailgunState.Charging;
		}
		else if(rState == RailgunState.Charging)
		{
			RaycastHit hit;
			Vector3 targetPoint = (projectileSpawn.forward * 100);
			if (Physics.Raycast(projectileSpawn.position, projectileSpawn.forward, out hit, Mathf.Infinity, LayerMask.NameToLayer("WallDeflection")))
			{
				targetPoint = hit.point;
			}
			
			railgunCharge += railgunChargeRate;
			
			railgunAim.enabled = true;
			railgunAim.SetPosition(0, projectileSpawn.position);
			railgunAim.SetPosition(1, (targetPoint));
			railgunAim.SetWidth(0.1f, (0.8f * (maxRailgunDamage/railgunCharge)));
			
			
			//Debug.Log("Charging");
			if(railgunCharge >= maxRailgunDamage)
			{
				railgunDamage = maxRailgunDamage;
				nextFire = Time.time + (fireRate/3);
				//rState = RailgunState.Firing;
				//Debug.Log("Fully Charged shot Fired: " + railgunDamage);
				FireWeapon(railgunDamage);
				railgunAim.enabled = false;
				
				rState = RailgunState.Waiting;
			}
		}
		
		if(rState == RailgunState.Firing)
		{
			FireWeapon(railgunDamage);
			railgunAim.enabled = false;
			
			rState = RailgunState.Waiting;
		}
	}

	void PlaySound(AudioSource source, int index)
	{
		source.clip = AudioClips [index];
		source.Play();
	}
	
	void FireWeapon(float shotDamage)
	{		
		Done_Mover shotScript;
		
		if(weaponType == "Shotgun")
		{
			int numshots = 4;
			var incr = shotgunSpread/numshots;
			Quaternion temp;
			float currAngle = -shotgunSpread/2f;
			
			for(; numshots > 0; numshots--)
			{
				temp = projectileSpawn.rotation * Quaternion.Euler(0,currAngle,0);
				currAngle += incr;
				projectileInstance = Instantiate (projectile, projectileSpawn.position, temp) as GameObject;
				shotScript = projectileInstance.GetComponent<Done_Mover>();
				shotScript.Damage = shotDamage;
				projectileInstance.gameObject.layer = LayerMask.NameToLayer(playerName + "Projectile");
				projectileInstance.gameObject.tag = "Shotgun";
				PlaySound(gunSource, 1);
			}
		}
		else if(weaponType == "MachineGun")
		{
			projectileInstance = Instantiate (projectile, projectileSpawn.position, projectileSpawn.rotation) as GameObject;
			shotScript = projectileInstance.GetComponent<Done_Mover>();
			shotScript.Damage = shotDamage;
			projectileInstance.gameObject.layer = LayerMask.NameToLayer(playerName + "Projectile");
			projectileInstance.gameObject.tag = "MachineGun";
			PlaySound(gunSource, 0);
		}
		else if(weaponType == "Railgun")
		{
			projectileInstance = Instantiate (projectile, projectileSpawn.position, projectileSpawn.rotation) as GameObject;
			shotScript = projectileInstance.GetComponent<Done_Mover>();
			shotScript.Damage = shotDamage;
			shotScript.Speed = 150;
			projectileInstance.gameObject.layer = LayerMask.NameToLayer(playerName + "Projectile");
			projectileInstance.gameObject.tag = "Railgun";
			PlaySound(gunSource, 2);
		}
		return;
	}

	void OnTriggerEnter (Collider other)
	{
		var tag = other.gameObject.tag;
		var shotScript = other.GetComponent<Done_Mover>();
			
		if (tag == "Shotgun" || tag == "MachineGun" || tag == "Railgun") 
		{
			Instantiate(shipHitEffect, this.transform.position, this.transform.rotation);
			PlaySound(otherSource, 4);
						
			if (currentShields <=  0)
			{
				currentHealth -= shotScript.Damage;
			}
			else
			{
				if(tag == "Railgun")
				{
					currentShields = 0;
					currentHealth -= shotScript.Damage;
				}
				else
					currentShields -= shotScript.Damage;

				shieldHitTime = Time.time;
			}

			Object.Destroy(other.gameObject);
		}
	}

	void OnCollisionEnter (Collision other)
	{
		var tag = other.gameObject.tag;
		Vector3 wallNormal;
		
		if (prevShields > 0 && currentShields <= 0)
		{
			Debug.Log ("PLAY SOUND");
			shieldSource.volume = 1f;
			PlaySound(shieldSource, 9);
		}


		//Set the normal vector based on what type of wall we hit so that the correct reflection angle is calculated.
		switch (tag) 
		{
			case "SideWall":
				PlaySound(otherSource, 3);

				//if we hit a wall, stop rotation
				if (shipState == ShipState.Rotating) 
				{
					currentGrapple = placeHolderGrapple;
					shipState = ShipState.Flying;
				}

				wallNormal = new Vector3(1, 0, 0);
				rigBody.velocity = Vector3.zero; //remove any forces from the ship
				direction = Vector3.Reflect (direction, wallNormal).normalized;
				transform.forward = direction;			
				Instantiate(wallCollisionEffect, this.transform.position, this.transform.rotation);
				rigBody.AddForce(direction * speed, ForceMode.VelocityChange);

				if (currentShields <=  0)
					currentHealth -= 15;
				else
				{
					currentShields = 0;
					shieldHitTime = Time.time;
				}

				break;

			case "TopWall":
				PlaySound(otherSource, 3);

				//if we hit a wall, stop rotation
				if (shipState == ShipState.Rotating) 
				{
					currentGrapple = placeHolderGrapple;
					shipState = ShipState.Flying;
				}

				wallNormal = new Vector3(0, 0, 1);
				rigBody.velocity = Vector3.zero; //remove any forces from the ship
				direction = Vector3.Reflect (direction, wallNormal).normalized;
				transform.forward = direction;
				rigBody.AddForce(direction * speed, ForceMode.VelocityChange);
				transform.forward = direction;			
				Instantiate(wallCollisionEffect, this.transform.position, this.transform.rotation);
				
				if (currentShields <= 0)
					currentHealth -= 15;
				else
				{
					currentShields = 0;
					shieldHitTime = Time.time;
				}

				break;
		}
	}
}
