﻿using UnityEngine;
using System.Collections;

public class Done_Mover : MonoBehaviour
{
	public float speed;
	public float damage;

	public float Damage
	{
		get {return damage;}
		set {damage = value;}
	}
	
	public float Speed
	{
		get {return speed;}
		set {speed = value;}
	}
	
	void Start ()
	{
		GetComponent<Rigidbody>().velocity = transform.forward * speed;
	}
}
